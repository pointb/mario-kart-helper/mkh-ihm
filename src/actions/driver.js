import {GET_DRIVERS_ERROR, GET_DRIVERS_RECEIVED_DATA, GET_DRIVERS_STARTED} from "./types";
import data from '../data/driver.json'


export const getAllDrivers = () => async (dispatch) => {
    dispatch({
        type: GET_DRIVERS_STARTED
    })

    try {
        return dispatch({
            type: GET_DRIVERS_RECEIVED_DATA,
            drivers: data
        })
    } catch (e) {
        dispatch({
            type: GET_DRIVERS_ERROR,
            error: e
        })
    }



}
