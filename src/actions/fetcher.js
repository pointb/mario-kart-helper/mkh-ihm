export const fetchData = async (url) => {

    const response = await fetch(url);

    if(response.status === 200) {
        return await response.json();
    }

    throw new Error(`Error with status code ${response.status}`)
}
