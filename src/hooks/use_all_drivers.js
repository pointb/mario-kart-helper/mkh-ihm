import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getAllDrivers} from "../actions/driver";

export const useAllDrivers = () => {
    const dispatch = useDispatch()

    const {drivers, hasReceivedDrivers, error} = useSelector(state => state.driverReducer);

    useEffect(() => {
        getAllDrivers()(dispatch)
    }, [dispatch])

    return {
        drivers,
        hasReceivedDrivers,
        error
    }
}
