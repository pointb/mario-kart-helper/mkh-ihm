import {GET_DRIVERS_STARTED, GET_DRIVERS_RECEIVED_DATA, GET_DRIVERS_ERROR} from "../actions/types";


const initialState = {
    hasReceivedDrivers: false,
    drivers: [],
    error: false
};

export const driverReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DRIVERS_STARTED:
            return {
                ...state,
                hasReceivedDrivers: null,
            };
        case GET_DRIVERS_RECEIVED_DATA:
            return {
                ...state,
                drivers: action.drivers,
                hasReceivedDrivers: true
            };
        case GET_DRIVERS_ERROR:
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};
