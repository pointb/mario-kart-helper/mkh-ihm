import {combineReducers} from 'redux';
import {driverReducer} from "./driver_reducer";


export default combineReducers({
    driverReducer
});
