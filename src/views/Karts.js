import React from "react";

import { Card, CardHeader, CardBody, Row, Col } from "reactstrap";

import karts from '../data/bodies.json'

const Karts = () => {
    return (
        <>
            <div className="content">
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                                <h5 className="title">{karts.length} Karts Mario Kart</h5>
                                <p className="category">
                                    Inspiré du jeu officiel{" "}
                                    <a href="https://nucleoapp.com/?ref=1712">Mario Kart 8 Deluxe</a>
                                </p>
                            </CardHeader>
                            <CardBody className="all-icons">
                                <Row>
                                    {karts.map(driver => (
                                        <Col
                                            key={driver.name}
                                            className="font-icon-list col-xs-6 col-xs-6"
                                            lg="2"
                                            md="3"
                                            sm="4"
                                        >
                                            <div className="font-icon-detail">
                                                <i className="tim-icons icon-delivery-fast" />
                                                <p>{driver?.name}</p>
                                            </div>
                                        </Col>
                                    ))}

                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        </>
    );
}

export default Karts;
